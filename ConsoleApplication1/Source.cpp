#include "MyGlWindow.h"
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Check_Button.H>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "EnumAlgoType.h"


MyGlWindow* gl;

Fl_Check_Button *globalWindAlgoActivation;
Fl_Check_Button *pointWindAlgoActivation;

Fl_Value_Input *windInputWindDirectionX;
Fl_Value_Input *windInputWindDirectionZ;

Fl_Output *windDirectionString;

Fl_Value_Input *windInputWindPositionX;
Fl_Value_Input *windInputWindPositionY;
Fl_Value_Input *windInputWindPositionZ;

Fl_Output *windPositionString;

Fl_Value_Slider *windSliderWindCircleRadius;

void idler(void * w)
{
	_int64 ctime;
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER *>(&ctime));
	int r = 60;  // FrameRate = 60 frames / sec

	if ((ctime - gl->_clock->etime) < (gl->_clock->freq / r))
		return;
	gl->_clock->stop();

	gl->updateElapsedTimeClock();

	gl->_clock->start();
}

static void Slider_CB(Fl_Widget *w, void *data) {
	gl->updateGrass(((Fl_Value_Slider*)w)->value());
}

static void InputValue_WindDirectionX(Fl_Widget *w, void *data)
{
	if (((Fl_Value_Input*)w)->value() >= -1 && ((Fl_Value_Input*)w)->value() <= 1)
		gl->updateWindDirectionX(((Fl_Value_Input*)w)->value());
}

/*static void InputValue_WindDirectionY(Fl_Widget *w, void *data)
{
	if (((Fl_Value_Input*)w)->value() >= -1 && ((Fl_Value_Input*)w)->value() <= 1)
		gl->updateWindDirectionY(((Fl_Value_Input*)w)->value());
}*/

static void InputValue_WindDirectionZ(Fl_Widget *w, void *data)
{
	if (((Fl_Value_Input*)w)->value() >= -1 && ((Fl_Value_Input*)w)->value() <= 1)
		gl->updateWindDirectionZ(((Fl_Value_Input*)w)->value());
}

static void InputValue_WindPositionX(Fl_Widget *w, void *data)
{
	gl->updateWindPositionX(((Fl_Value_Input*)w)->value());
}

static void InputValue_WindPositionY(Fl_Widget *w, void *data)
{
	gl->updateWindPositionY(((Fl_Value_Input*)w)->value());
}

static void InputValue_WindPositionZ(Fl_Widget *w, void *data)
{
	gl->updateWindPositionZ(((Fl_Value_Input*)w)->value());
}

void hidePointWindAlgoParameters()
{
	windInputWindPositionX->hide();
	windInputWindPositionX->redraw();

	windInputWindPositionY->hide();
	windInputWindPositionY->redraw();

	windInputWindPositionZ->hide();
	windInputWindPositionZ->redraw();

	windPositionString->hide();
	windPositionString->redraw();

	windSliderWindCircleRadius->hide();
	windSliderWindCircleRadius->redraw();
}

void displayPointWindAlgoParameters()
{
	windInputWindPositionX->set_visible();
	windInputWindPositionX->redraw();

	windInputWindPositionY->set_visible();
	windInputWindPositionY->redraw();

	windInputWindPositionZ->set_visible();
	windInputWindPositionZ->redraw();

	windPositionString->set_visible();
	windPositionString->redraw();

	windSliderWindCircleRadius->set_visible();
	windSliderWindCircleRadius->redraw();
}

void hideGlobalWindAlgoParameters()
{
	windInputWindDirectionX->hide();
	windInputWindDirectionX->redraw();

	windInputWindDirectionZ->hide();
	windInputWindDirectionZ->redraw();

	windDirectionString->hide();
	windDirectionString->redraw();
}

void displayGlobalWindAlgoParameters()
{
	windInputWindDirectionX->set_visible();
	windInputWindDirectionX->redraw();

	windInputWindDirectionZ->set_visible();
	windInputWindDirectionZ->redraw();

	windDirectionString->set_visible();
	windDirectionString->redraw();
}

static void CheckBoxValue_PointWindAlgo(Fl_Widget *w, void *data)
{
	if (((Fl_Check_Button*)w)->value() == true)
	{
		globalWindAlgoActivation->value(false);

		hideGlobalWindAlgoParameters();
		displayPointWindAlgoParameters();

		gl->updateAlgoUsedToSimulatedWind(WindAlgoType::PointWindAlgo);
	}
	else
	{
		globalWindAlgoActivation->value(true);

		hidePointWindAlgoParameters();
		displayGlobalWindAlgoParameters();

		gl->updateAlgoUsedToSimulatedWind(WindAlgoType::GlobalWindAlgo);
	}
}

static void CheckBoxValue_GlobalWindAlgo(Fl_Widget *w, void *data)
{
	if (((Fl_Check_Button*)w)->value() == true)
	{
		pointWindAlgoActivation->value(false);

		hidePointWindAlgoParameters();
		displayGlobalWindAlgoParameters();

		gl->updateAlgoUsedToSimulatedWind(WindAlgoType::GlobalWindAlgo);
	}
	else
	{
		pointWindAlgoActivation->value(true);

		hideGlobalWindAlgoParameters();
		displayPointWindAlgoParameters();

		gl->updateAlgoUsedToSimulatedWind(WindAlgoType::PointWindAlgo);
	}
}

static void Button_ResetTimeStamp(Fl_Widget *w, void *data)
{
	gl->setTime(0);
}

static void Slider_WindCircleRadius(Fl_Widget *w, void *data)
{
	gl->updateWindCircleRadius(((Fl_Value_Slider*)w)->value());
}

static void Slider_WindFrequency(Fl_Widget *w, void *data)
{
	gl->updateWindFrequency(((Fl_Value_Slider*)w)->value());
}

static void Slider_WindStrength(Fl_Widget *w, void *data)
{
	gl->updateWindStrength(((Fl_Value_Slider*)w)->value());
}

int main()
{
	Fl::scheme("plastic");	// plastic
	int width = 900;
	int height = 700;
	Fl_Double_Window* wind = new Fl_Double_Window(100, 100, width, height, "GL 3D FrameWork");

	wind->begin();	// put widgets inside of the window
	gl = new MyGlWindow(10, 10, width - 220, height - 20);
	wind->resizable(gl);

	Fl_Value_Slider *grassSlider = new Fl_Value_Slider(width - 200, 20, 190, 20, "Grass numbers");
	grassSlider->maximum(10000);
	grassSlider->minimum(100);
	grassSlider->value(5000);
	grassSlider->type(FL_HORIZONTAL);
	grassSlider->callback(Slider_CB);

	globalWindAlgoActivation = new Fl_Check_Button(width - 190, 100, 100, 20, "Activate Global wind algo");
	globalWindAlgoActivation->value(true);
	globalWindAlgoActivation->type(FL_HORIZONTAL);
	globalWindAlgoActivation->callback(CheckBoxValue_GlobalWindAlgo);

	pointWindAlgoActivation = new Fl_Check_Button(width - 190, 120, 100, 20, "Activate Point wind algo");
	pointWindAlgoActivation->value(false);
	pointWindAlgoActivation->type(FL_HORIZONTAL);
	pointWindAlgoActivation->callback(CheckBoxValue_PointWindAlgo);

	// wind direction
	windInputWindDirectionX = new Fl_Value_Input(width - 160, 160, 30, 20, "X : ");
	windInputWindDirectionX->maximum(1);
	windInputWindDirectionX->minimum(-1);
	windInputWindDirectionX->value(0);
	windInputWindDirectionX->type(FL_HORIZONTAL);
	windInputWindDirectionX->callback(InputValue_WindDirectionX);

/*	Fl_Value_Input *windInputWindDirectionY = new Fl_Value_Input(width - 120, 100, 30, 20, "Y : ");
	windInputWindDirectionY->maximum(1);
	windInputWindDirectionY->minimum(-1);
	windInputWindDirectionY->value(0);
	windInputWindDirectionY->type(FL_HORIZONTAL);
	windInputWindDirectionY->callback(InputValue_WindDirectionY);*/

	windInputWindDirectionZ = new Fl_Value_Input(width - 100, 160, 30, 20, "Z : ");
	windInputWindDirectionZ->maximum(1);
	windInputWindDirectionZ->minimum(-1);
	windInputWindDirectionZ->value(0);
	windInputWindDirectionZ->type(FL_HORIZONTAL);
	windInputWindDirectionZ->callback(InputValue_WindDirectionZ);

	windDirectionString = new Fl_Output(width - 150, 190, 100, 20);
	windDirectionString->static_value("Wind direction");

	// wind position
	windInputWindPositionX = new Fl_Value_Input(width - 180, 160, 30, 20, "X : ");
	windInputWindPositionX->maximum(1);
	windInputWindPositionX->minimum(-1);
	windInputWindPositionX->value(0);
	windInputWindPositionX->type(FL_HORIZONTAL);
	windInputWindPositionX->callback(InputValue_WindPositionX);
	windInputWindPositionX->hide();


	windInputWindPositionY = new Fl_Value_Input(width - 120, 160, 30, 20, "Y : ");
	windInputWindPositionY->maximum(1);
	windInputWindPositionY->minimum(-1);
	windInputWindPositionY->value(0);
	windInputWindPositionY->type(FL_HORIZONTAL);
	windInputWindPositionY->callback(InputValue_WindPositionY);
	windInputWindPositionY->hide();

	windInputWindPositionZ = new Fl_Value_Input(width - 60, 160, 30, 20, "Z : ");
	windInputWindPositionZ->maximum(1);
	windInputWindPositionZ->minimum(-1);
	windInputWindPositionZ->value(0);
	windInputWindPositionZ->type(FL_HORIZONTAL);
	windInputWindPositionZ->callback(InputValue_WindPositionZ);
	windInputWindPositionZ->hide();

	windPositionString = new Fl_Output(width - 150, 190, 100, 20);
	windPositionString->static_value("Wind position");
	windPositionString->hide();

	Fl_Button *resetTimestampButton = new Fl_Button(width - 190, 220, 120, 20);
	resetTimestampButton->label("Reset timestamp");
	globalWindAlgoActivation->type(FL_HORIZONTAL);
	resetTimestampButton->callback(Button_ResetTimeStamp);

	Fl_Value_Slider *windSliderFrequency = new Fl_Value_Slider(width - 200, 260, 190, 20, "Wind frequency");
	windSliderFrequency->maximum(5.0f);
	windSliderFrequency->minimum(0.1f);
	windSliderFrequency->value(1.0f);
	windSliderFrequency->type(FL_HORIZONTAL);
	windSliderFrequency->callback(Slider_WindFrequency);

	Fl_Value_Slider *windSliderWindStrength = new Fl_Value_Slider(width - 200, 300, 190, 20, "Wind Strength");
	windSliderWindStrength->maximum(0.5f);
	windSliderWindStrength->minimum(0.1f);
	windSliderWindStrength->value(0.1f);
	windSliderWindStrength->type(FL_HORIZONTAL);
	windSliderWindStrength->callback(Slider_WindStrength);

	windSliderWindCircleRadius = new Fl_Value_Slider(width - 200, 340, 190, 20, "Wind Circle radius");
	windSliderWindCircleRadius->maximum(20.0f);
	windSliderWindCircleRadius->minimum(1.0f);
	windSliderWindCircleRadius->value(5.0f);
	windSliderWindCircleRadius->type(FL_HORIZONTAL);
	windSliderWindCircleRadius->callback(Slider_WindCircleRadius);
	windSliderWindCircleRadius->hide();

	wind->show();	// this actually opens the window

	Fl::add_idle((void(*)(void*)) idler, gl);
	Fl::run();

	delete wind;
	return 1;
}