#version 400

struct MaterialInfo {
  vec3 rgb;           // Normal Color
  vec3 specRgb;      // Specular reflectivity
  float shiness;
};

struct LightInfo {
  vec4 position; 
  vec3 intensity;
  float ambientCoefficient;
  float attenuation;
  float coneAngle;
  vec3 direction;
};

in float Z;
in vec2 TexCoord;
in vec3 N;
in vec4 P;
in vec3 V;

uniform float lnbr;
uniform LightInfo Light[10];

uniform MaterialInfo Material;

out vec4 FragColor; //Final color

const vec3 fogColor = vec3(0.5f,0.5f,0.5f);
const float maxDist = 100;
const float minDist = 50;
const float FogDensity = 0.01;

uniform sampler2D texture_d;

//Light compute
vec3 ApplyLight(LightInfo light, vec3 normal, vec4 surfacePos, vec3 surfaceToCamera, vec3 texColor) {
	vec3 surfaceToLight;
	float attenuation = 1.0;
	
    if (light.position.w == 0.0) {
        //directional light
        surfaceToLight = normalize(vec3(light.position));
        attenuation = 1.0;
    } else {
		//Point Light - Spot Light
		surfaceToLight = normalize(vec3(light.position - surfacePos));

		//Point light
		float distanceToLight = length(vec3(light.position - surfacePos));
        attenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));

		//cone restrictions
		if (light.coneAngle != 0) {
			float ang = degrees(acos(dot(normalize(-surfaceToLight), light.direction)));
			if (ang >= light.coneAngle || ang < 0) { 
				attenuation = 0.0f;
			}
		}
	}

 	//ambient
    vec3 ambient = light.ambientCoefficient * texColor * light.intensity;

	//diffuse
	float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));
    vec3 diffuse = diffuseCoefficient * texColor * light.intensity;

	//specular
    float specularCoefficient = 0.0;
    if(diffuseCoefficient > 0.0)
        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, normalize(reflect(-1 * surfaceToLight,normal)) )), Material.shiness);
    vec3 specular = specularCoefficient * Material.specRgb * light.intensity;

	return ambient + attenuation * (diffuse + specular);
}

void main() {
	if(textureSize(texture_d, 0).x < 1.0)
		discard;

	vec4 texColor = texture(texture_d, TexCoord);

	if(texColor.a < 0.1) // if alpha = 0 dont' compute light
		discard;

	vec3 finalColor;

	for (int i = 0; i < lnbr; i++) {
		finalColor += ApplyLight(Light[i], N, P, V, vec3(texColor)); //Color Computation
	}

	//fog compute
	vec3 gamma = vec3(1.0/2.2, 1.0/2.2, 1.0/2.2);
	vec3 color = vec3(pow(finalColor.r, gamma.r),
					  pow(finalColor.g, gamma.g),
					  pow(finalColor.b, gamma.b));

	float fogFactor = (maxDist - Z)/(maxDist - minDist);
    fogFactor = clamp( fogFactor, 0.0, 1.0 );

	finalColor = mix(fogColor, finalColor, fogFactor); // Fog apply

    FragColor = vec4(finalColor, 1); //Final color return
}