#pragma once

#include "IObject.h"

using namespace std;

class Skybox : public AObject
{
public:
	Skybox();
	~Skybox();

	void setup(ShaderProgram *shader);
	void draw(ModelView model, glm::mat4 view, glm::mat4 projection, vector<Light*> l_light);

private:
	void initShader();
	GLuint textureID;
};

