#version 440

layout (location = 0) in vec4 VertexPosition; 
layout (location = 1) in vec3 VertexNormal;
layout (location = 2) in vec2 texCoords;

struct MaterialInfo {
  vec3 rgb;           // Normal Color
  vec3 specRgb;      // Specular reflectivity
  float shiness;
};

struct LightInfo {
  vec4 position; 
  vec3 intensity;
  float ambientCoefficient;
  float attenuation;
  float coneAngle;
  vec3 direction;
};

uniform float lnbr;
uniform LightInfo Light[10];

uniform MaterialInfo Material;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix; 
uniform mat4 MVP;

out vec3 LightIntensity;
out float Z;
out vec2 TexCoord;

vec3 ApplyLight(LightInfo light, vec3 normal, vec4 surfacePos, vec3 surfaceToCamera) {
	vec3 surfaceToLight;
	float attenuation = 1.0;

    if (light.position.w == 0.0) {
        //directional light
        surfaceToLight = normalize(vec3(light.position));
        attenuation = 1.0;
    } else {
		//Point Light - Spot Light
		surfaceToLight = normalize(vec3(light.position - surfacePos));

		//Point light
		float distanceToLight = length(vec3(light.position - surfacePos));
        attenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));

		//cone restrictions
		if (light.coneAngle != 0) {
			float ang = degrees(acos(dot(normalize(-surfaceToLight), light.direction)));
			if (ang >= light.coneAngle || ang < 0) { 
				attenuation = 0.0f;
			}
		}
	}

 	//ambient
    vec3 ambient = light.ambientCoefficient * Material.rgb * light.intensity;

	//diffuse
	float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));
    vec3 diffuse = diffuseCoefficient * Material.rgb * light.intensity;

	//specular
    float specularCoefficient = 0.0;
    if(diffuseCoefficient > 0.0)
        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, normalize(reflect(-1 * surfaceToLight,normal)) )), Material.shiness);
    vec3 specular = specularCoefficient * Material.specRgb * light.intensity;

	return ambient + attenuation * (diffuse + specular);
}

void main()
{
	vec3 N = normalize(NormalMatrix * VertexNormal);
	vec4 P = ModelViewMatrix * VertexPosition; 
	vec3 V = normalize(-1 * vec3(P));

	vec3 finalColor = vec3(0,0,0);

	for (int i = 0; i < lnbr; i++) {
		finalColor += ApplyLight(Light[i], N, P, V);
	}

	LightIntensity = finalColor;
	gl_Position = MVP * VertexPosition;
	Z = abs(P.z);
	TexCoord = texCoords;
}

