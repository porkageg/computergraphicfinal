#include "Transform.h"

Transform::Transform(glm::vec3 pos, float d, glm::vec3 rot, glm::vec3 scale)
{
	position = pos;
	degree = d;
	rotation = rot;
	this->scale = scale;
}

Transform::~Transform()
{
}

