
#pragma once

#include <iostream>
#include <vector>

#include "loader.h"

#include "ModelView.h"
#include "Transform.h"
#include "Light.h"

#include "GL/glew.h"
#include <GL/gl.h>

#include <glm/gtc/type_ptr.hpp>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"

#include "SOIL\SOIL.h"

class AObject {
public:
	virtual void draw(ModelView model, glm::mat4 view, glm::mat4 projection, std::vector<Light *> l_light);
	virtual void setup(ShaderProgram *shaderProgram);

	ShaderProgram *shaderProgram;

	GLuint vaoHandle;
	Transform *transform;
	GLuint tex_2d[2];
};