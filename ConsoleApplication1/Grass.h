#pragma once

#include "IObject.h"
#include "EnumAlgoType.h"

using namespace std;

class Grass : public AObject
{
public:
	Grass(string path, int amount);
	~Grass();

	void setup(ShaderProgram *shader);
	void draw(ModelView model, glm::mat4 view, glm::mat4 projection, vector<Light*> l_light);
	void setupShader();

	void setAmount(int amount);
	void setElapsedTimeClock(float elapsedTimeClock);
	void setWindDirectionX(float windDirectionX);
	void setWindDirectionY(float windDirectionY);
	void setWindDirectionZ(float windDirectionZ);
	void setWindPositionX(float windPositionX);
	void setWindPositionY(float windPositionY);
	void setWindPositionZ(float windPositionZ);
	void setWindFrequency(float windFrequency);
	void setWindStrength(float windStrength);
	void setCircleRadius(float circleRadius);
	void setAlgoUsedToSimulatedWind(WindAlgoType windAlgoType);
private:
	string _path; // Path to the grass texture
	GLuint textureID;
	GLuint b_grass_tranform_gen;

	int _amount;
	float _elapsedTime;
	glm::vec3 _windDirection;
	glm::vec3 _windPosition;
	float _circleRadius;
	float _windStrength;
	float _windFrequency;
	WindAlgoType _windAlgoType;
};

