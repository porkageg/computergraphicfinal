#pragma once

#include "GL/glew.h"
#include <GL/gl.h>
#include <glm/gtc/type_ptr.hpp>

class Transform
{
public:
	Transform(glm::vec3 pos = glm::vec3(0,0,0), float d = 0.f, glm::vec3 rot = glm::vec3(0, 0, 0), glm::vec3 scale = glm::vec3(1, 1, 1));
	~Transform();

	glm::vec3 position;
	float degree;
	glm::vec3 rotation;
	glm::vec3 scale;

};

