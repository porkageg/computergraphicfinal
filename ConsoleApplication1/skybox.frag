#version 400

in vec3 TexCoords;
out vec4 FragColor; //Final color

uniform samplerCube skybox;

void main()
{    
    FragColor = texture(skybox, TexCoords);
}