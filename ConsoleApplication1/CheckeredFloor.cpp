#include "CheckeredFloor.h"

CheckeredFloor::CheckeredFloor()
{
	this->color1 = glm::vec3(0, 1, 0);
	this->color2 = glm::vec3(0, 1, 0);
	this->size = 20;
	this->nSquares = 20;
	transform = new Transform();
}

CheckeredFloor::CheckeredFloor(glm::vec3 color1, glm::vec3 color2, int size, int nSquares) {
	this->color1 = color1;
	this->color2 = color2;
	this->size = size;
	this->nSquares = nSquares;
}

CheckeredFloor::~CheckeredFloor()
{
}

void CheckeredFloor::setup(ShaderProgram *shader)
{
	shaderProgram = shader;
	AObject::setup(shader);

	glGenVertexArrays(1, &vaoHandle);
	glBindVertexArray(vaoHandle); // (activated)

	float maxX = size / 2.f, maxY = size / 2.f;
	float minX = -size / 2.f, minY = -size / 2.f;
	int x, y, v[3], i;
	float xp, yp, xd, yd;
	v[2] = 0;
	xd = (maxX - minX) / ((float)nSquares);
	yd = (maxY - minY) / ((float)nSquares);

	for (x = 0, xp = minX; x<nSquares; x++, xp += xd) {
		for (y = 0, yp = minY, i = x; y<nSquares; y++, i++, yp += yd) {
			if (i % 2 == 0) {
				clists.push_back(color1);
				clists.push_back(color1);
				clists.push_back(color1);
				clists.push_back(color1);
				clists.push_back(color1);
				clists.push_back(color1);
			}
			else {
				clists.push_back(color2);
				clists.push_back(color2);
				clists.push_back(color2);
				clists.push_back(color2);
				clists.push_back(color2);
				clists.push_back(color2);
			}
			vlists.push_back(glm::vec3(xp, -0.5, yp));
			vlists.push_back(glm::vec3(xp, -0.5, yp + yd));
			vlists.push_back(glm::vec3(xp + xd, -0.5, yp + yd));

			vlists.push_back(glm::vec3(xp, -0.5, yp));
			vlists.push_back(glm::vec3(xp + xd, -0.5, yp + yd));
			vlists.push_back(glm::vec3(xp + xd, -0.5, yp));
		}
	}

	GLuint vbo_plane_vertices;
	glGenBuffers(1, &vbo_plane_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_plane_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* vlists.size() * 3, vlists.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexPosition"),
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		0);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexPosition"));

	GLuint vbo_plane_colors;
	glGenBuffers(1, &vbo_plane_colors);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_plane_colors);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * clists.size() * 3, clists.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexNormal"),
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		0);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexNormal"));

	// Vertex Texture Coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindVertexArray(0);
}

void CheckeredFloor::draw(ModelView model, glm::mat4 view, glm::mat4 projection, std::vector<Light *> l_light)
{
	model.glPushMatrix();
	shaderProgram->use();

	model.glTranslate(transform->position.x, transform->position.y, transform->position.z);
	if (transform->degree != 0.0f)
		model.glRotate(transform->degree, transform->rotation.x, transform->rotation.y, transform->rotation.z);
	model.glScale(transform->scale.x, transform->scale.y, transform->scale.z);

	glm::mat4 mvp = projection * view * model.getMatrix();
	glm::mat4 modelview = view * model.getMatrix();
	glm::mat4 inverseModelView = glm::inverse(modelview);
	glm::mat3 normalMatrix = glm::mat3(glm::transpose(inverseModelView));

	glm::vec3 rgb(1, 1, 1);
	glm::vec3 specRgb(0.9f, 0.9f, 0.9f);
	GLfloat shiness = 180.0f;
	int i = 0;

	glUniform1f(shaderProgram->uniform("lnbr"), l_light.size());

	for (std::vector<Light *>::iterator it = l_light.begin(); it != l_light.end(); it++) {

		glUniform4fv(shaderProgram->uniform("Light[" + std::to_string(i) + "].position"), 1, glm::value_ptr(view * (*it)->position));
		glUniform3fv(shaderProgram->uniform("Light[" + std::to_string(i) + "].intensity"), 1, glm::value_ptr((*it)->intensity));
		glUniform1f(shaderProgram->uniform("Light[" + std::to_string(i) + "].ambientCoefficient"), (*it)->ambientCoefficient);
		glUniform1f(shaderProgram->uniform("Light[" + std::to_string(i) + "].attenuation"), (*it)->attenuation);
		glUniform1f(shaderProgram->uniform("Light[" + std::to_string(i) + "].coneAngle"), (*it)->coneAngle);
		glUniform3fv(shaderProgram->uniform("Light[" + std::to_string(i) + "].direction"), 1, glm::value_ptr(view * glm::vec4(glm::normalize((*it)->direction), 0)));
		i++;
	}

	glUniform3fv(shaderProgram->uniform("Material.rgb"), 1, glm::value_ptr(rgb));
	glUniform3fv(shaderProgram->uniform("Material.specRgb"), 1, glm::value_ptr(specRgb));
	glUniform1fv(shaderProgram->uniform("Material.shiness"), 1, &shiness);

	glUniformMatrix4fv(shaderProgram->uniform("ModelViewMatrix"), 1, GL_FALSE, glm::value_ptr(modelview));  //modelView
	glUniformMatrix3fv(shaderProgram->uniform("NormalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));  //normalMatrix
	glUniformMatrix4fv(shaderProgram->uniform("MVP"), 1, GL_FALSE, glm::value_ptr(mvp));  //mvp

	glBindVertexArray(vaoHandle);
	glDrawArrays(GL_TRIANGLES, 0, vlists.size());
	glBindVertexArray(0);

	model.glPopMatrix();
	shaderProgram->disable();
}
