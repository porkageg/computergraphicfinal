#version 400

in vec3 LightIntensity;
in float Z;
in vec2 TexCoord;

out vec4 FragColor;

const vec3 fogColor = vec3(0.5f,0.5f,0.5f);
const float maxDist = 100;
const float minDist = 20;
const float FogDensity = 0.01;

void main() {
	vec3 gamma = vec3(1.0/2.2, 1.0/2.2, 1.0/2.2);
	vec3 color = vec3(pow(LightIntensity.r, gamma.r),
					  pow(LightIntensity.g, gamma.g),
					  pow(LightIntensity.b, gamma.b));

	float fogFactor = (maxDist - Z)/(maxDist - minDist);
    fogFactor = clamp( fogFactor, 0.0, 1.0 );

	vec3 finalColor = mix(fogColor, color, fogFactor);

	vec4 resColor = vec4(finalColor, 1);
	
    FragColor = resColor;
}
