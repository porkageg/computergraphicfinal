#include "Light.h"

Light::Light(glm::vec4 position, glm::vec3 intensity, float coneAngle, glm::vec3 direction, float attenuation, float ambientCoefficient)
{
	this->position = position;
	this->intensity = intensity;
	this->attenuation = attenuation;
	this->coneAngle = coneAngle;
	this->ambientCoefficient = ambientCoefficient;
	this->direction = direction;
}

Light::~Light()
{
}
