#pragma once

#include <windows.h>
#include <iostream>

#include "loader.h"
#include "GL/glew.h"
#include <GL/gl.h>
#include <string>

#include <FL/Fl_Gl_Window.h>
#include <Fl/Fl.h>

#include <glm/gtc/type_ptr.hpp>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"

#include "ModelView.h"
#include "Viewer.h"

#include "IObject.h"
#include "CheckeredFloor.h"
#include "Transform.h"

#include "Grass.h"
#include "Skybox.h"

#include "Timing.h"

#include "EnumAlgoType.h"

class MyGlWindow : public Fl_Gl_Window {
public:
	MyGlWindow(int x, int y, int w, int h);
	void setup();

	void updateGrass(double amount);
	void updateElapsedTimeClock();
	void updateWindDirectionX(float windDirectionX);
	void updateWindDirectionY(float windDirectionY);
	void updateWindDirectionZ(float windDirectionZ);
	void updateWindPositionX(float windPositionX);
	void updateWindPositionY(float windPositionY);
	void updateWindPositionZ(float windPositionZ);
	void updateWindCircleRadius(float circleRadius);
	void updateAlgoUsedToSimulatedWind(WindAlgoType windAlgoType);
	void updateWindFrequency(float windFrequency);
	void updateWindStrength(float windStrength);

	void setTime(double time);
	double getTime() const;

	Clock * _clock;
private:
	void initialize();
	void draw();
	int handle(int);
	void initShader();

	Viewer *m_viewer;
	ModelView m_model;
	int first;
	ShaderProgram *lightShader;
	ShaderProgram *modelShader;
	
	std::vector<AObject *> l_object;
	std::vector<Light *> l_light;
	Grass* _grass;
	Skybox* _skybox;

	bool _isGrassInitialize;
	double _time;

	WindAlgoType _windAlgoType;
};
