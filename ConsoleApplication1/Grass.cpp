#include "Grass.h"



Grass::Grass(string path, int amount)
{
	_path = path;
	_amount = amount;
	_elapsedTime = 0.0f;
	_windDirection.x = 1.0f;
	_windDirection.y = 0.0f;
	_windDirection.z = 0.0f;
	_windPosition.x = 0.0f;
	_windPosition.y = 0.0f;
	_windPosition.z = 0.0f;
	_circleRadius = 5.0f;
	_windFrequency = 1.0f;
	_windStrength = 0.1f;
	_windAlgoType = WindAlgoType::GlobalWindAlgo;
}


Grass::~Grass()
{
}

void Grass::setupShader() {
	shaderProgram = new ShaderProgram();
	shaderProgram->initFromFiles("grass.vert", "grass.frag");

	//Main attribute
	shaderProgram->addAttribute("VertexPosition");
	shaderProgram->addAttribute("VertexNormal");
	//Add Texture Coordinate attribuite
	shaderProgram->addAttribute("texCoords");
	shaderProgram->addAttribute("instanceMatrix");

	//Normal & camera input
	shaderProgram->addUniform("projection");
	shaderProgram->addUniform("view");

	//Light parameter
	shaderProgram->addUniform("lnbr");
	for (int i = 0; i < 10; i++)
	{
		std::string name = "Light[";

		std::string pos = name + std::to_string(i) + "].position";
		std::string intensity = name + std::to_string(i) + "].intensity";
		std::string ambient = name + std::to_string(i) + "].ambientCoefficient";
		std::string attenuation = name + std::to_string(i) + "].attenuation";
		std::string coneAngle = name + std::to_string(i) + "].coneAngle";
		std::string direction = name + std::to_string(i) + "].direction";

		shaderProgram->addUniform(pos);
		shaderProgram->addUniform(intensity);
		shaderProgram->addUniform(ambient);
		shaderProgram->addUniform(attenuation);
		shaderProgram->addUniform(coneAngle);
		shaderProgram->addUniform(direction);
	};

	shaderProgram->addUniform("texture_d");

	//Material set
	shaderProgram->addUniform("Material.specRgb");
	shaderProgram->addUniform("Material.shiness");

	//elapsed time clock set
	shaderProgram->addUniform("elapsedTimeClock");

	//wind direction set
	shaderProgram->addUniform("windDirection");

	//wind position set
	shaderProgram->addUniform("windPosition");

	//wind circle radius
	shaderProgram->addUniform("circleRadius");

	//wind Frequency set
	shaderProgram->addUniform("windFrequency");

	//wind Strength set
	shaderProgram->addUniform("windStrength");

	//wind algo type set
	shaderProgram->addUniform("windAlgoType");
}

void Grass::setAmount(int amount)
{
	if (amount < 0)
		_amount = 0;
	else 
		_amount = amount;

	// gen grass positions - tab of mat4 model
	glm::mat4* modelMatrices = new glm::mat4[_amount];
	srand(time_t(NULL)); // init random
	GLfloat radius = 5.0;
	GLfloat offset = 6.5f;
	for (GLuint i = 0; i < _amount; i++)
	{
		glm::mat4 model;

		GLfloat angle = (GLfloat)i / (GLfloat)_amount * 360.0f;
		GLfloat displacement = (rand() % (GLint)(2 * offset * 2)) / 2 - offset;
		GLfloat x = sin(angle) * radius + displacement;

		GLfloat y = 0; // y no deplacement

		displacement = (rand() % (GLint)(2 * offset * 2)) / 2 - offset;
		GLfloat z = cos(angle) * radius + displacement;
		model = glm::translate(model, glm::vec3(x, y, z));

		model = glm::scale(model, glm::vec3(1, 1, 1));

		modelMatrices[i] = model;
	}

	//instance model buffer init
	glBindVertexArray(vaoHandle);
	glBindBuffer(GL_ARRAY_BUFFER, b_grass_tranform_gen);
	glBufferData(GL_ARRAY_BUFFER, _amount * sizeof(glm::mat4), &modelMatrices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(shaderProgram->attribute("instanceMatrix"));
	glVertexAttribPointer(shaderProgram->attribute("instanceMatrix"), 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)0);
	glEnableVertexAttribArray(shaderProgram->attribute("instanceMatrix") + 1);
	glVertexAttribPointer(shaderProgram->attribute("instanceMatrix") + 1, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(sizeof(glm::vec4)));
	glEnableVertexAttribArray(shaderProgram->attribute("instanceMatrix") + 2);
	glVertexAttribPointer(shaderProgram->attribute("instanceMatrix") + 2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(2 * sizeof(glm::vec4)));
	glEnableVertexAttribArray(shaderProgram->attribute("instanceMatrix") + 3);
	glVertexAttribPointer(shaderProgram->attribute("instanceMatrix") + 3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(3 * sizeof(glm::vec4)));

	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);
	glVertexAttribDivisor(5, 1);
	glVertexAttribDivisor(6, 1);

	glEnableVertexAttribArray(shaderProgram->attribute("instanceMatrix"));
	glBindVertexArray(0);
}

void Grass::setElapsedTimeClock(float elapsedTimeClock)
{
	_elapsedTime = elapsedTimeClock;
}

void Grass::setWindDirectionX(float windDirectionX)
{
	_windDirection.x = windDirectionX;
}

void Grass::setWindDirectionY(float windDirectionY)
{
	_windDirection.y = windDirectionY;
}

void Grass::setWindDirectionZ(float windDirectionZ)
{
	_windDirection.z = windDirectionZ;
}

void Grass::setWindPositionX(float windPositionX)
{
	_windPosition.x = windPositionX;
}

void Grass::setWindPositionY(float windPositionY)
{
	_windPosition.y = windPositionY;
}

void Grass::setWindPositionZ(float windPositionZ)
{
	_windPosition.z = windPositionZ;
}

void Grass::setCircleRadius(float circleRadius)
{
	_circleRadius =  circleRadius;
}

void Grass::setWindFrequency(float windFrequency)
{
	_windFrequency = windFrequency;
}

void Grass::setWindStrength(float windStrength)
{
	_windStrength = windStrength;
}

void Grass::setAlgoUsedToSimulatedWind(WindAlgoType windAlgoType)
{
	_windAlgoType = windAlgoType;
}

void Grass::setup(ShaderProgram * shader)
{
	//VAO - VBO
	setupShader();

	glGenVertexArrays(1, &vaoHandle);
	glBindVertexArray(vaoHandle);

	vector<glm::vec4> vlists;
	vector<glm::vec3> nlists;

	//First
	vlists.push_back(glm::vec4(1, -0.5f, 0, 1.0f));
	vlists.push_back(glm::vec4(0, -0.5f, 0, 1.0f));
	vlists.push_back(glm::vec4(0, 0.5f, 0, 1.0f));

	vlists.push_back(glm::vec4(0, 0.5f, 0, 1.0f));
	vlists.push_back(glm::vec4(1, 0.5f, 0, 1.0f));
	vlists.push_back(glm::vec4(1, -0.5f, 0, 1.0f));

	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));

	//Second
	vlists.push_back(glm::vec4(0.25, 0.5f, 0.5f, 1.0f));
	vlists.push_back(glm::vec4(0.25, -0.5f, 0.5f, 1.0f));
	vlists.push_back(glm::vec4(0.75, -0.5f, -0.5f, 1.0f));

	vlists.push_back(glm::vec4(0.75, 0.5f, -0.5f, 1.0f));
	vlists.push_back(glm::vec4(0.25, 0.5f, 0.5f, 1.0f));
	vlists.push_back(glm::vec4(0.75, -0.5f, -0.5f, 1.0f));

	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));

	//Last
	vlists.push_back(glm::vec4(0.75, 0.5f, 0.5f, 1.0f));
	vlists.push_back(glm::vec4(0.25, 0.5f, -0.5f, 1.0f));
	vlists.push_back(glm::vec4(0.25, -0.5f, -0.5f, 1.0f));

	vlists.push_back(glm::vec4(0.75, 0.5f, 0.5f, 1.0f));
	vlists.push_back(glm::vec4(0.25, -0.5f, -0.5f, 1.0f));
	vlists.push_back(glm::vec4(0.75, -0.5f, 0.5f, 1.0f));

	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));
	nlists.push_back(glm::vec3(0, 1, 0));

	//get Texture
	glGenTextures(1, &textureID);

	int width, height;
	unsigned char* image = SOIL_load_image(_path.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);

	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);	// Use GL_CLAMP_TO_EDGE to prevent semi-transparent borders. Due to interpolation it takes value from next repeat 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(image);

	GLuint vbo_grass_vertices;
	glGenBuffers(1, &vbo_grass_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_grass_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* vlists.size() * 4, vlists.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexPosition"),
		4,
		GL_FLOAT,
		GL_FALSE,
		0,
		0);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexPosition"));

	GLuint vbo_grass_normal;
	glGenBuffers(1, &vbo_grass_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_grass_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * nlists.size() * 3, nlists.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexNormal"),
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		0);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexNormal"));


	float tex[18 * 2] = {

		//front
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f,1.0f,

		//Second
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f,0.0f,
		0.0f, 1.0f,

		//Last
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,

		0.0f, 0.0f,
		1.0f,1.0f,
		0.0f, 1.0f,
	};


	GLuint texbuffer;
	glGenBuffers(1, &texbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, texbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 18 * 2, tex, GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("texCoords"),                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glEnableVertexAttribArray(shaderProgram->attribute("texCoords"));

	// buffer generated grass position
	glGenBuffers(1, &b_grass_tranform_gen);

	glBindVertexArray(0);

	setAmount(_amount);

 //unbounding the VAO
}

void Grass::draw(ModelView model, glm::mat4 view, glm::mat4 projection, vector<Light*> l_light)
{
	model.glPushMatrix();
	shaderProgram->use();

	glm::vec3 rgb(1, 1, 1);
	glm::vec3 specRgb(1, 1, 1);
	GLfloat shiness = 180;
	int i = 0;

	glUniform1f(shaderProgram->uniform("lnbr"), l_light.size());

	for (std::vector<Light *>::iterator it = l_light.begin(); it != l_light.end(); it++) {

		glUniform4fv(shaderProgram->uniform("Light[" + std::to_string(i) + "].position"), 1, glm::value_ptr(view * (*it)->position));
		glUniform3fv(shaderProgram->uniform("Light[" + std::to_string(i) + "].intensity"), 1, glm::value_ptr((*it)->intensity));
		glUniform1f(shaderProgram->uniform("Light[" + std::to_string(i) + "].ambientCoefficient"), (*it)->ambientCoefficient);
		glUniform1f(shaderProgram->uniform("Light[" + std::to_string(i) + "].attenuation"), (*it)->attenuation);
		glUniform1f(shaderProgram->uniform("Light[" + std::to_string(i) + "].coneAngle"), (*it)->coneAngle);
		glUniform3fv(shaderProgram->uniform("Light[" + std::to_string(i) + "].direction"), 1, glm::value_ptr(view * glm::vec4(glm::normalize((*it)->direction), 0)));
		i++;
	}

	glUniform3fv(shaderProgram->uniform("Material.specRgb"), 1, glm::value_ptr(specRgb));
	glUniform1fv(shaderProgram->uniform("Material.shiness"), 1, &shiness);

	glUniformMatrix4fv(shaderProgram->uniform("projection"), 1, GL_FALSE, glm::value_ptr(projection)); 
	glUniformMatrix4fv(shaderProgram->uniform("view"), 1, GL_FALSE, glm::value_ptr(view)); 

	glUniform1fv(shaderProgram->uniform("elapsedTimeClock"), 1, &_elapsedTime);
	glUniform3fv(shaderProgram->uniform("windDirection"), 1, glm::value_ptr(_windDirection));
	glUniform3fv(shaderProgram->uniform("windPosition"), 1, glm::value_ptr(_windPosition));
	glUniform1fv(shaderProgram->uniform("circleRadius"), 1, &_circleRadius);
	glUniform1fv(shaderProgram->uniform("windFrequency"), 1, &_windFrequency);
	glUniform1fv(shaderProgram->uniform("windStrength"), 1, &_windStrength);
	glUniform1i(shaderProgram->uniform("windAlgoType"), _windAlgoType);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE); // Enable front cull face - Camera see only front grass
	glCullFace(GL_FRONT);

	glActiveTexture(GL_TEXTURE0 + textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glUniform1i(shaderProgram->uniform("texture_d"), textureID);

	glBindVertexArray(vaoHandle);

	glDrawArraysInstanced(GL_TRIANGLES, 0, 18, _amount); // Draw _amount instance of grass

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);

	glDisable(GL_CULL_FACE);

	model.glPopMatrix();
	shaderProgram->disable();
}
