#include "IObject.h"
 
class CheckeredFloor : public AObject
{
public:
	CheckeredFloor();
	CheckeredFloor(glm::vec3 color1, glm::vec3 color2, int size, int nSquares);
	~CheckeredFloor();

	void setup(ShaderProgram *shader);
	void draw(ModelView model, glm::mat4 view, glm::mat4 projection, std::vector<Light*> l_light);
private:
	GLuint vbo_cube_vertices, vbo_cube_colors;
	
	glm::vec3 color1;
	glm::vec3 color2;
	int size; 
	int nSquares;

	std::vector<glm::vec3> vlists;
	std::vector<glm::vec3> clists;
};

