#version 440

layout (location = 0) in vec4 VertexPosition; 
layout (location = 1) in vec3 VertexNormal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in mat4 instanceMatrix;

uniform mat4 projection;
uniform mat4 view;
uniform float elapsedTimeClock;
uniform vec3 windDirection;
uniform vec3 windPosition;
uniform float circleRadius;
uniform float windFrequency;
uniform float windStrength;
uniform int windAlgoType;

out float Z;
out vec2 TexCoord;
out	vec3 N;
out	vec4 P;
out	vec3 V;

mat4 mvp;
mat4 modelview;

#define PointWindAlgo		0
#define GlobalWindAlgo		1
#define M_PI 3.1415926535897932384626433832795

vec3 calcWindAnimationTranslation()
{
	vec3 dir = normalize(windDirection);
	float power = windStrength;
	vec3 pos = vec3(instanceMatrix * VertexPosition);
	return (vec3(VertexPosition) * dir * power * sin((pos.x * windDirection.x + pos.z * windDirection.z  )+ (2 * M_PI / (5.01 - windFrequency)) * elapsedTimeClock));
}

vec3 wPos = vec3(0, 0.2, 0);
vec3 calcWindAnnimationFromPoint() {

	vec4 p1 = instanceMatrix * VertexPosition; // Grass World position 
	vec4 p2 = vec4(windPosition, 1); // Wind emitor position
	
	float dist = distance(p1, p2);
	float strength =  windFrequency - windFrequency * dist / circleRadius; // strength at grass position
	vec3 dir = normalize(vec3(p1 - p2)); // Wind Direction (vec3(grass position - Wind Emitor Position))

	if (strength < 0.0f)
		strength = 0.0f;
	float power = strength / windFrequency * windStrength;

	return (vec3(VertexPosition) * dir * power * sin(dist + (2 * M_PI / (5.01 - windFrequency)) * elapsedTimeClock));
}

void main()
{
	mvp = projection * view * instanceMatrix;
	modelview = view * instanceMatrix;
	mat3 normalMatrix = mat3(transpose(inverse(modelview)));

	// Compute Wind effect

	vec3 vertexTranslation = vec3(0, 0, 0);

	if (windAlgoType == GlobalWindAlgo)
	{
		vertexTranslation = calcWindAnimationTranslation();
	}
	else if (windAlgoType == PointWindAlgo)
	{
		vertexTranslation = calcWindAnnimationFromPoint();
	}

	vec3 vpos = vec3(VertexPosition) + vertexTranslation;
	vec3 vnorm = VertexNormal + vertexTranslation;

	N = normalize(normalMatrix * vnorm);
	P = modelview * vec4(vpos, 1.0f);
	V = normalize(-1 * vec3(P));

  	gl_Position = mvp * vec4(vpos, 1.0f);
	Z = abs(P.z);
	TexCoord = texCoords;
}

