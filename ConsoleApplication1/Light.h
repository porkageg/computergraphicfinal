#pragma once

#include "Transform.h"

class Light
{

public:
	Light(glm::vec4 position, glm::vec3 intensity, float coneAngle = 0.f, glm::vec3 direction = glm::vec3(0,-1,0), float attenuation = 0.05f, float ambientCoefficient = 0.01f);
	~Light();

	glm::vec4 position;
	glm::vec3 intensity;
	float attenuation;
	float coneAngle;
	float ambientCoefficient;
	glm::vec3 direction;
};

