#include "MyGlWindow.h"
#include "Debug.h"

static double DEFAULT_VIEW_POINT[3] = { 5, 5, 5 };
static double DEFAULT_VIEW_CENTER[3] = { 0, 0, 0 };
static double DEFAULT_UP_VECTOR[3] = { 0, 1, 0 };

MyGlWindow::MyGlWindow(int x, int y, int w, int h)
	: Fl_Gl_Window(x, y, w, h)
{
	mode(FL_RGB | FL_ALPHA | FL_DOUBLE | FL_STENCIL);
	first = 0;
	glm::vec3 viewPoint(DEFAULT_VIEW_POINT[0], DEFAULT_VIEW_POINT[1], DEFAULT_VIEW_POINT[2]);
	glm::vec3 viewCenter(DEFAULT_VIEW_CENTER[0], DEFAULT_VIEW_CENTER[1], DEFAULT_VIEW_CENTER[2]);
	glm::vec3 upVector(DEFAULT_UP_VECTOR[0], DEFAULT_UP_VECTOR[1], DEFAULT_UP_VECTOR[2]);
	double aspect = (w / h);
	m_viewer = new Viewer(viewPoint, viewCenter, upVector, 45.f, aspect);
	_clock = new Clock();
	_isGrassInitialize = false;
	_time = 0.00001f;
	_windAlgoType = WindAlgoType::PointWindAlgo;
}

void MyGlWindow::initialize()
{
	initShader();

	l_object.push_back(new CheckeredFloor());

	_grass = new Grass("./Ressources/grass.png", 5000);
	l_object.push_back(_grass);
	_skybox = new Skybox();

	l_light.push_back(new Light(glm::vec4(0, 10, 10, 0), glm::vec3(1, 1, 1)));

	l_light.push_back(new Light(glm::vec4(0, 3, 0, 1), glm::vec3(0.8f, 0.8f, 0.8f), 40));
	l_light.push_back(new Light(glm::vec4(-2, 3, 4, 1), glm::vec3(0.1f,0.1f, 1), 40));
	l_light.push_back(new Light(glm::vec4(3, 3, -3, 1), glm::vec3(1, 0.1f, 0.1f),40));

	l_light.push_back(new Light(glm::vec4(-5, 1, -5, 1), glm::vec3(1.0f, 0.4f, 0.8f)));
}

void MyGlWindow::initShader()
{
	// init Light Shader
	lightShader = new ShaderProgram();
	lightShader->initFromFiles("light.vert", "light.frag");

	//Main attribute
	lightShader->addAttribute("VertexPosition");
	lightShader->addAttribute("VertexNormal");
	//Add Texture Coordinate attribuite
	lightShader->addAttribute("texCoords");

	//Normal & camera input
	lightShader->addUniform("ModelViewMatrix");
	lightShader->addUniform("NormalMatrix");
	lightShader->addUniform("MVP");

	//Light parameter
	lightShader->addUniform("lnbr");
	for (int i = 0; i < 10; i++)
	{
		std::string name = "Light[";

		std::string pos = name + std::to_string(i) + "].position";
		std::string intensity = name + std::to_string(i) + "].intensity";
		std::string ambient = name + std::to_string(i) + "].ambientCoefficient";
		std::string attenuation = name + std::to_string(i) + "].attenuation";
		std::string coneAngle = name + std::to_string(i) + "].coneAngle";
		std::string direction = name + std::to_string(i) + "].direction";

		lightShader->addUniform(pos);
		lightShader->addUniform(intensity);
		lightShader->addUniform(ambient);
		lightShader->addUniform(attenuation);
		lightShader->addUniform(coneAngle);
		lightShader->addUniform(direction);
	};

	//Material set
	lightShader->addUniform("Material.rgb");
	lightShader->addUniform("Material.specRgb");
	lightShader->addUniform("Material.shiness");
}

void MyGlWindow::setup()
{
	_skybox->setup(lightShader);
	
	for each (AObject* object in l_object)
	{
		object->setup(lightShader);
	}
	_isGrassInitialize = true;
}

void MyGlWindow::updateGrass(double amount)
{
	_grass->setAmount(amount);
	redraw();
}

void MyGlWindow::updateElapsedTimeClock()
{
	if (_isGrassInitialize)
	{
		_time += _clock->get();
		_grass->setElapsedTimeClock((float) _time);
		redraw();
	}
}

void MyGlWindow::updateWindDirectionX(float windDirectionX)
{
	_grass->setWindDirectionX(windDirectionX);
	redraw();
}

void MyGlWindow::updateWindDirectionY(float windDirectionY)
{
	_grass->setWindDirectionY(windDirectionY);
	redraw();
}

void MyGlWindow::updateWindDirectionZ(float windDirectionZ)
{
	_grass->setWindDirectionZ(windDirectionZ);
	redraw();
}

void MyGlWindow::updateWindPositionX(float windPositionX)
{
	_grass->setWindPositionX(windPositionX);
	redraw();
}

void MyGlWindow::updateWindPositionY(float windPositionY)
{
	_grass->setWindPositionY(windPositionY);
	redraw();
}

void MyGlWindow::updateWindPositionZ(float windPositionZ)
{
	_grass->setWindPositionZ(windPositionZ);
	redraw();
}

void MyGlWindow::updateWindCircleRadius(float circleRadius)
{
	_grass->setCircleRadius(circleRadius);
	redraw();
}

void MyGlWindow::updateWindFrequency(float windFrequency)
{
	_grass->setWindFrequency(windFrequency);
	redraw();
}

void MyGlWindow::updateWindStrength(float windStrength)
{
	_grass->setWindStrength(windStrength);
	redraw();
}

void MyGlWindow::setTime(double time)
{
	_time = time;
	redraw();
}

double MyGlWindow::getTime() const
{
	return (_time);
}

void MyGlWindow::updateAlgoUsedToSimulatedWind(WindAlgoType windAlgoType)
{
	_grass->setAlgoUsedToSimulatedWind(windAlgoType);
	redraw();
}

void MyGlWindow::draw(void)
{
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (!first)
	{
		glewExperimental = TRUE;
		GLenum err = glewInit();
		if (err != GLEW_OK)
		{
			std::cout << "glewInit failed, aborting." << std::endl;
		}
		first = 1;
		initialize();
		setup();
		glDebugMessageCallback(Core::DebugOutput::myCallback, NULL);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE,
			GL_DONT_CARE, 0, NULL, GL_TRUE);
	}
	glEnable(GL_DEPTH_TEST);
	glViewport(0, 0, w(), h());

	glm::vec3 p = m_viewer->getViewPoint();
	glm::vec3 d = m_viewer->getViewCenter();
	glm::vec3 u = m_viewer->getUpVector();
	glm::mat4 projection = glm::perspective(45.0f, h() / (float)w(), 0.1f, 5000.0f);

	glm::mat4 view = glm::mat4(glm::mat3(glm::lookAt(p, d, u)));
	_skybox->draw(m_model, view, projection, l_light);

	view = glm::lookAt(p, d, u);

	for each (AObject* object in l_object)
	{
		object->draw(m_model, view, projection, l_light);
	}
}

int m_pressedMouseButton;
int m_lastMouseX;
int m_lastMouseY;

int MyGlWindow::handle(int e)
//==========================================================================
{
	switch (e) {
	case FL_SHOW:        // you must handle this, or not be seen!
		show();
		return 1;
	case FL_PUSH:
	{

		m_pressedMouseButton = Fl::event_button();

		//      m_viewer->setAspectRatio( static_cast<double>(this->w()) / static_cast<double>(this->h()) );

		m_lastMouseX = Fl::event_x();
		m_lastMouseY = Fl::event_y();
	}
	damage(1);
	return 1;
	case FL_RELEASE:
		m_pressedMouseButton = -1;
		damage(1);
		return 1;
	case FL_DRAG: // if the user drags the mouse
	{

		double fractionChangeX = static_cast<double>(Fl::event_x() - m_lastMouseX) / static_cast<double>(this->w());
		double fractionChangeY = static_cast<double>(m_lastMouseY - Fl::event_y()) / static_cast<double>(this->h());

		if (m_pressedMouseButton == 1) {
			m_viewer->rotate(fractionChangeX, fractionChangeY);
		}
		else if (m_pressedMouseButton == 2) {
			m_viewer->zoom(fractionChangeY);
		}
		else if (m_pressedMouseButton == 3) {
			m_viewer->translate(-fractionChangeX, -fractionChangeY, (Fl::event_key(FL_Shift_L) == 0) || (Fl::event_key(FL_Shift_R) == 0));
		}
		else {
			std::cout << "Warning: dragging with unknown mouse button!  Nothing will be done" << std::endl;
		}

		m_lastMouseX = Fl::event_x();
		m_lastMouseY = Fl::event_y();
		redraw();
	}

	return 1;

	case FL_KEYBOARD:
		return 0;

	default:
		return 0;
	}
}
